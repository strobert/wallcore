'use strict'
const request = require('request')
const fs = require('fs')
const express = require('express')
const cmd = require('node-cmd')
var configIni = require('ini')
var crypto = require('crypto')
const execSync = require('child_process').execSync;
const app = express()

const logs = require('./libs/Logs')

//DEVICE LOAD CONFIG
var config; //global config var
var VERSION="0.0.8";
var DEVICE_TYPE;
var DEVICE_IP;
var DEVICE_PORT;
var DEVICE_ID;
var DEVICE_ACCOUNT_ID;
var DEVICE_PLAYLIST_ID;
var DEVICE_NAME;
var DEVICE_DESC;
var FOLDER_TMP;
var FOLDER_VIDEOS;
var DEVICE_TIME_REQUEST=25000;
//CENTRAL SERVER CONFIG
var URL_CENTRAL_SERVER_API;
var URL_CENTRAL_SERVER_API_CONFIG;



function checksum(str, algorithm, encoding) {
  return crypto
    .createHash(algorithm || 'md5')
    .update(str, 'utf8')
    .digest(encoding || 'hex')
}

//LOAD CONFIG
function setConfigFile(){
  var configFileDefault='DEVICE_TYPE = "SLAVE" \n'+
  'DEVICE_IP = "127.0.0.1" \n'+
  'DEVICE_PORT = "8080" \n'+
  'DEVICE_ID = "1" \n'+
  'DEVICE_ACCOUNT_ID = "1" \n'+
  'DEVICE_PLAYLIST_ID = "1" \n'+
  'FOLDER_TMP = "tmp" \n'+
  'FOLDER_VIDEOS = "videos" \n'+
  'DEVICE_TIME_REQUEST = "25000" \n'+
  'URL_CENTRAL_SERVER_API = "http://robervega.com/wallcore/api/api.php"\n'+
  'URL_CENTRAL_SERVER_API_CONFIG = "http://robervega.com/wallcore/api/apiconfig.php"'
  ;
  try{
    fs.writeFile('config.ini', configFileDefault, function (err) {
      if (err) {
        logs.showErrorLog("Cant create config.ini");
        throw err;
        process.exit();
      }
      logs.showSuccessLog('config.ini default CREATED!');
    logs.showLog('Please restart WallCore.');
    process.exit();

    });
  }catch(e){
    logs.showErrorLog("Cant create config.ini "+e);
    process.exit();
  }
}

function setPlaylist(playlist){
  var playlistText='';

  playlist.forEach(video => {
      playlistText+=video+"\n";  
  });

  try{
    fs.writeFile('playlist.pls', playlistText, function (err) {
      if (err) {
        logs.showErrorLog("Cant update playlist.pls");
        throw err;
        process.exit();
      }
    logs.showSuccessLog('playlist.pls UPDATED!');
    });
  }catch(e){
    logs.showErrorLog("Cant update playlist.pls "+e);
  }
}

function setlastUpdate(){
  var lastupdateFile='CHECKSUM = '+currentResponse.data.videosChecksum+' \n';
      lastupdateFile+='DATE = "'+currentResponse.data.date+'" ';
  try{
    fs.writeFile('lastupdate.ini', lastupdateFile, function (err) {
      if (err) {
        logs.showErrorLog("Cant write lastupdate.ini");
        throw err;
      }
      console.log('lastupdate.ini Saved! with new CHECKSUM "'+currentResponse.data.videosChecksum+'" ');
    });
  }catch(e){
    logs.showErrorLog("Cant set last upload checksum "+e);
  }
}

function resetlastUpdate(){
  var lastupdateFile='CHECKSUM =  \n';
      lastupdateFile+='DATE =  ';
  try{
    fs.writeFile('lastupdate.ini', lastupdateFile, function (err) {
      if (err) {
        logs.showErrorLog("Cant reset lastupdate.ini");
        throw err;
      }
      console.log('lastupdate.ini reseted! with empty CHECKSUM ');
    });
  }catch(e){
    logs.showErrorLog("Cant reset lastupdate.ini checksum "+e);
  }
}

function resetRequestVars(){
  requestInProgress=false;
  downloadsInProgress=0;
  downloadsInProgressTotal=0;
  validateInProgress=0;
  validateInProgressTotal=0;
  validateLocalInProgressTotal=0;
  validateLocalInProgress=0;
  islocalValidating=false;
}

/*CHECKSUMS*/
var validateInProgressTotal=0;
var validateInProgress=0;
var validateLocalInProgressTotal=0;
var validateLocalInProgress=0;
var localVideosCheckSum="";
var islocalValidating=false;

/*REQUESTS VARS*/
var millisecsInterval=10000;
var reqs = 0
var requestInProgress=false;
var downloadsInProgressTotal=0;
var downloadsInProgress=0;
var timeoutRequest=15000;
var currentResponse;
var requestConfigInProgress=false;

//TIMERS

var timerIntervalConfig;
var timerInterval;


//REQUESTS
function requestCentralServer(){
  reqs++;
  logs.showRequestsLog("checking news from central server for Device "+DEVICE_ID+" ("+reqs+")..");
  requestInProgress=true;
  var options={
    url: URL_CENTRAL_SERVER_API,
    timeout: timeoutRequest,
    method: 'POST',
    json:{
      "DEVICE_ID":DEVICE_ID,
      "DEVICE_ACCOUNT_ID":DEVICE_ACCOUNT_ID,
      "DEVICE_PLAYLIST_ID":DEVICE_PLAYLIST_ID
    },
    headers: {
        'content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'User-Agent': 'wallcore-client'
    }
  };
    request(options, function(err, res, body) {  
      requestInProgress=false;
      if(!err){
        logs.showRequestsLog("Response from Central Server");
        var response = body;
        currentResponse=response;
        try {
          if(response.status=="SUCCESS"){
            logs.showSuccessLog("REQUEST CENTRAL SERVER");
            var lastUpdate;
            try{
              if(lastUpdate=configIni.parse(fs.readFileSync('./lastupdate.ini', 'utf-8'))){
                if(lastUpdate.CHECKSUM==response.data.videosChecksum){
                  logs.showLog("SAME VIDEOS FROM CENTRAL SERVER. DEVICE "+DEVICE_NAME+" UP-TO-DATE");
                  setPlaylist(response.data.playlist);
                }else if(response.data.videosChecksum==false || response.data.videos.length==0) {
                  logs.showLog("NO VIDEOS FROM CENTRAL SERVER.");
                  removeVideos();
                }else{ //start download new videos
                  logs.showLog(response.message);
                  setPlaylist(response.data.playlist);
                  initDownloadVideos(response.data.videos);
                }
              }else{
                logs.showRequestsLog("First download from central server");
                if(response.data.videosChecksum==false || response.data.videos.length==0) {
                  logs.showLog("NO VIDEOS FROM CENTRAL SERVER.");
                  removeVideos();
                }else{ //start download new videos
                  logs.showLog(response.message);
                  setPlaylist(response.data.playlist);
                  initDownloadVideos(response.data.videos);
                }
              }
            }catch(e){
              logs.showRequestsLog("No lastupdate.ini file. First download from central server");
              setPlaylist(response.data.playlist);
              initDownloadVideos(response.data.videos);
            }
          }else{
            logs.showErrorLog("Error from central server: "+response.message);
          }
        }catch(e){
          logs.showErrorLog("Internal Error from central server: "+e);
        }
      }else{
        logs.showErrorLog("No response or Internal server error from central server: "+err);
      }
     
  });
}


function requestConfig(){
  logs.showConfigLog("Getting config from central server for Device id "+DEVICE_ID+"");
  requestConfigInProgress=true;
  var options={
    url: URL_CENTRAL_SERVER_API_CONFIG,
    timeout: timeoutRequest,
    method: 'POST',
    json:{
      "DEVICE_ID":DEVICE_ID,
      "DEVICE_ACCOUNT_ID":DEVICE_ACCOUNT_ID
    },
    headers: {
        'content-type': 'application/json',
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'User-Agent': 'wallcore-client'
    }
  };
    request(options, function(err, res, body) {  
      requestConfigInProgress=false;
      if(!err){
        logs.showConfigLog(" Response from Central Server");
        var configResponse = body;
        try {
          if(configResponse.status=="SUCCESS"){
            logs.showConfigLog(" REQUEST CENTRAL SERVER");
            logs.showConfigLog(configResponse.message+" UPDATED");
            DEVICE_ACCOUNT_ID=configResponse.data.DEVICE_ACCOUNT_ID;
            DEVICE_PLAYLIST_ID=configResponse.data.DEVICE_PLAYLIST_ID;
            DEVICE_TIME_REQUEST=configResponse.data.DEVICE_TIME_REQUEST;
            //clearInterval(timerInterval);
            //timerInterval = setInterval(requestInterval,DEVICE_TIME_REQUEST);
          }else{
            logs.showConfigLog(" Error from central server: "+configResponse.message);
          }
        }catch(e){
          logs.showConfigLog(" Internal Error from central server: "+e);
        }
      }else{
        logs.showConfigLog(" No response or Internal server error from central server: "+err);
      }
     
  });
}


const requestIntervalConfig = function(){
  if(!requestConfigInProgress &&!islocalValidating && !requestInProgress && downloadsInProgress==downloadsInProgressTotal && validateInProgress==validateInProgress){
    requestConfig();
  }
};

const requestInterval = function(){
  if(!requestConfigInProgress &&!islocalValidating && !requestInProgress && downloadsInProgress==downloadsInProgressTotal && validateInProgress==validateInProgress){
    initValidateLocalVideos();
  }
};


//REST
app.get('/', (request, response) => {
  logs.showRequestsLog("A client request from /");
  var html_logs="";

  html_logs+="<h3>Var logs wallcore</h3><br>";
  html_logs+="Requests = "+reqs+"<br>";
  html_logs+="requestInProgress = "+requestInProgress+"<br>";
  html_logs+="downloadsInProgressTotal = "+downloadsInProgressTotal+"<br>";
  html_logs+="validateInProgress = "+validateInProgress+"<br>";
  html_logs+="validateInProgressTotal = "+validateInProgressTotal+"<br>";
  html_logs+="validateLocalInProgressTotal = "+validateLocalInProgressTotal+"<br>";
  html_logs+="validateLocalInProgress = "+validateLocalInProgress+"<br>";
  html_logs+="islocalValidating = "+islocalValidating+"<br>";

  response.send(html_logs);
})
app.get('/restartSlave', (request, response) => {
  var html_logs="";
  html_logs+="<h3>Wallcore</h3><br>";
  html_logs+="Restarting player in slave<br><br>";


  if(DEVICE_TYPE!="MASTER"){

    logs.showRequestsLog("A client request from /restartSlave");
    const strBash= `
    killall pwomxplayer.bin
    pwomxplayer --config=my_config udp://239.0.1.23:1234?buffer_size=1200000B
  `;
    cmd.get(strBash,function(err, data, stderr){ 
      if(!err){
        logs.showLog('Restarting player in slave');
      }else{
        logs.showErrorLog("Can't restart player: "+err);
      }  
    });
    try{
      response.send("Restart slave ok");
    }catch(e){
      logs.showErrorLog("Error restart slave:"+e);
    }
  }else{
    response.send(html_logs+"Error 403: Devices is type "+DEVICE_TYPE);
  }

})




//WALLCORE INIT AND SERVER METHODS

function initDownloadVideos (videos){
  if(videos != null){
    downloadsInProgressTotal = videos.length;
    downloadsInProgress = 0;
    validateInProgressTotal = videos.length;
    validateInProgress = 0;
    logs.showRequestsLog("Initializing download from central server.");
    cmd.get('mkdir '+FOLDER_TMP,function(err, data, stderr){ });


    videos.forEach(video => {
     cmdDownloadVideo(video);
    });
    logs.showRequestsLog("Downloading "+downloadsInProgressTotal+" Videos, please wait...");

  }else{
    logs.showErrorLog("No videos in response data");
  }
}



function cmdDownloadVideo(video){
    cmd.get('wget '+video.videoUrl+' -O ./'+FOLDER_TMP+'/'+video.videoFile+' ',
        function(err, data, stderr){
          downloadsInProgress++;
            //logs.showLog(stderr);
            if (!err) {
              logs.showSuccessLog('Video download complete! ('+downloadsInProgress+'/'+downloadsInProgressTotal+') ');
              if(downloadsInProgress==downloadsInProgressTotal){
                logs.showSuccessLog('VIDEOS DOWNLOADS COMPLETE!');
                initValidateVideos();
              }
            } else {
              logs.showErrorLog('error on downloading ('+video.videoUrl+') : '+err);
              downloadsInProgress=0;
              downloadsInProgressTotal=0;
            }
        }
    );
}
function initValidateVideos(){
  if(currentResponse.data.videos != null){
    var videos=currentResponse.data.videos;
    logs.showLog("Validating "+validateInProgressTotal+" videos in "+DEVICE_NAME+", please wait...");
    videos.forEach(video => {
      validateVideo(video);
    });
    
  }else{
    logs.showErrorLog("No videos in responce data");
  }
}
function listDir (path){
      let files = fs.readdirSync( path );
      let filesWithStats = [];
      if( files.length > 1 ) {
          let sorted = files.sort((a, b) => {
          let s1 = fs.statSync( path +"/"+ a);
          let s2 = fs.statSync(  path +"/"+ b);
          return s1.ctime < s2.ctime;
      });
      sorted.forEach(file => {
          filesWithStats.push({
              filename: file,
              date: new Date(fs.statSync( path+ "/" + file).ctime),
              path: path + file
          });
      });
    } else {
      files.forEach(file => {
          filesWithStats.push({
              filename: file,
              date: new Date(fs.statSync(path + "/"+file).ctime),
              path: path + file
          });
      });
    }
    return filesWithStats;
}

function initValidateLocalVideos(){
  cmd.get('mkdir '+FOLDER_VIDEOS,function(err, data, stderr){ });
  var lastUpdate;
  islocalValidating=true;
  try{
    if(lastUpdate=configIni.parse(fs.readFileSync('./lastupdate.ini', 'utf-8'))){
        try{
          var files=listDir("./"+FOLDER_VIDEOS);
          if(files.length==0){
            localVideosCheckSum="";
            validateLocalInProgress=0;
            validateLocalInProgressTotal=0;
            islocalValidating=false;
            logs.showLog("Local validation: No videos in folder");
            resetlastUpdate();
            requestCentralServer();
          }else{
            var str_videos="";
            validateLocalInProgressTotal=files.length;
            var str_files_array = [];
            files.forEach(file => {
              str_files_array.push(file.filename+"");
            });
            for (let i = 0; i < str_files_array.length; i++) {
              const file = str_files_array[i];
              try {
                var dataVideo=fs.readFileSync('./'+FOLDER_VIDEOS+'/'+file);
                validateLocalInProgress++;
                str_videos+=checksum(dataVideo);
                      //logs.showLog("Local validation: getting local video checksum ("+validateLocalInProgress+"/"+validateLocalInProgressTotal+") "+file);
                      if(validateLocalInProgress==validateLocalInProgressTotal){
                        localVideosCheckSum=checksum(str_videos);
                        if(localVideosCheckSum==lastUpdate.CHECKSUM){
                          validateLocalInProgress=0;
                          validateLocalInProgressTotal=0;
                          logs.showSuccessLog("Local validation: videos files validated successfully!");
                          islocalValidating=false;
                          localVideosCheckSum="";
                          requestCentralServer();
                        }else{
                          logs.showErrorLog("Local validation: Invalid checksum in local videos LOCAL CHECKSUM="+localVideosCheckSum+" , restarting request.");
                          islocalValidating=false;
                          validateLocalInProgress=0;
                          validateLocalInProgressTotal=0;
                          resetlastUpdate();
                          localVideosCheckSum="";
                          requestCentralServer();
                        }
                      }
              }catch(e){
                logs.showErrorLog('Local validation: cant get data checksum from local video '+file+' , maybe the video dont exist locally.: '+e);
                validateLocalInProgress=0;
                validateLocalInProgressTotal=0;
                islocalValidating=false;
                resetlastUpdate();
                localVideosCheckSum="";
                requestCentralServer();
              }
            }
          }
        }catch(e){
            validateLocalInProgress=0;
            validateLocalInProgressTotal=0;
            islocalValidating=false;
            localVideosCheckSum="";
            logs.showLog("Local validation: No /"+FOLDER_VIDEOS+" folder, starting request");
            requestCentralServer();
        }
    }else{
      validateLocalInProgress=0;
      validateLocalInProgressTotal=0;
      islocalValidating=false;
      localVideosCheckSum="";
      logs.showLog("Local validation: No lastupdate.ini, starting request");
      requestCentralServer();
    }
  }catch(e){
    validateLocalInProgress=0;
    validateLocalInProgressTotal=0;
    islocalValidating=false;
    localVideosCheckSum="";
    logs.showLog("Local validation: No lastupdate.ini, starting request..");
    requestCentralServer();
  }
}

function updateFolders(){
  logs.showLog('Deleting old folder ./'+FOLDER_VIDEOS);
  cmd.get('rm -rf ./'+FOLDER_VIDEOS,function(err, data, stderr){ 
    if(!err){
      logs.showLog('Renaming folder ./'+FOLDER_TMP+' to '+'./'+FOLDER_VIDEOS);
      fs.renameSync('./'+FOLDER_TMP,'./'+FOLDER_VIDEOS);
      logs.showSuccessLog('New videos update done! :)');
      setlastUpdate();
      resetRequestVars(); //restart the cycle
    }else{
      logs.showErrorLog("Can't delete folder ./"+FOLDER_VIDEOS+" : "+err);
      validateInProgress=0;
      validateInProgressTotal=0;
    }
  });
}

function removeVideos(){
  logs.showLog('Deleting folders ./'+FOLDER_VIDEOS);
  cmd.get('rm -rf ./'+FOLDER_VIDEOS,function(err, data, stderr){ 
    cmd.get('rm -rf ./'+FOLDER_TMP,function(err, data, stderr){ 
      setlastUpdate();
      resetRequestVars(); //restart the cycle
      setPlaylist([]);

    });
  });
  
  

}
function validateVideo(video){
  fs.readFile('./'+FOLDER_TMP+'/'+video.videoFile, function(err, dataVideo) {
    validateInProgress++;
    if(!err){
      if(video.videoChecksum===checksum(dataVideo)){
        logs.showSuccessLog('Video validated succefully! ('+validateInProgress+'/'+validateInProgressTotal+') '+video.videoFile);
        if(validateInProgress==validateInProgressTotal){   
          logs.showSuccessLog('New videos validated 100%');
          setPlaylist(currentResponse.data.playlist);
          updateFolders();
        }
      }else{
        logs.showErrorLog('Invalid checksum video '+video.videoFile+' , maybe the video is corrupted');
        validateInProgress=0;
        validateInProgressTotal=0;
      }
    }else{
      logs.showErrorLog('cant validate video '+video.videoFile+' , maybe the video dont exist locally.');
      validateInProgress=0;
      validateInProgressTotal=0;
    }
  })
}

  function initWallCore(){ // init master or slave wallcore

    try{
      if(config=configIni.parse(fs.readFileSync('./config.ini', 'utf-8'))){
        DEVICE_TYPE=config.DEVICE_TYPE;
        DEVICE_IP=config.DEVICE_IP;
        DEVICE_ID=config.DEVICE_ID;
        DEVICE_PORT=config.DEVICE_PORT;
        DEVICE_ACCOUNT_ID=config.DEVICE_ACCOUNT_ID;
        DEVICE_PLAYLIST_ID=config.DEVICE_PLAYLIST_ID;
        DEVICE_NAME=DEVICE_TYPE+"_"+DEVICE_ID;
        DEVICE_DESC="Device "+DEVICE_NAME+" type "+DEVICE_TYPE+" with device ID "+DEVICE_ID;
        URL_CENTRAL_SERVER_API=config.URL_CENTRAL_SERVER_API;
        URL_CENTRAL_SERVER_API_CONFIG=config.URL_CENTRAL_SERVER_API_CONFIG;
        DEVICE_TIME_REQUEST = config.DEVICE_TIME_REQUEST;
        
        FOLDER_TMP=config.FOLDER_TMP;
        FOLDER_VIDEOS=config.FOLDER_VIDEOS;

        logs.showLog("#####################################");
        logs.showLog("########## WALLCORE "+VERSION+" ###########");
        logs.showLog("#####################################");
        logs.showLog("DEVICE TYPE: "+DEVICE_TYPE);
        logs.showLog("DEVICE NAME: "+DEVICE_NAME);
        logs.showLog("DEVICE IP: "+DEVICE_IP);
        logs.showLog("DEVICE PORT: "+DEVICE_PORT);
        logs.showLog("DEVICE ID: "+DEVICE_ID);
        logs.showLog("DEVICE ACCOUNT ID: "+DEVICE_ACCOUNT_ID);
        logs.showLog("DEVICE PLAYLIST ID: "+DEVICE_PLAYLIST_ID);
        logs.showLog("URL CENTRAL SERVER API: "+URL_CENTRAL_SERVER_API);
        logs.showLog("URL CENTRAL SERVER CONFIG: "+URL_CENTRAL_SERVER_API_CONFIG);
        logs.showLog(DEVICE_DESC);
        logs.showLog("#####################################");
        logs.showLog("Initializing....");
    
        if(DEVICE_TYPE=="MASTER"){
          app.listen(DEVICE_PORT, (err) => {
            if (err) {
              return logs.showErrorLog('something bad happened on initialize server: '+err);
            }
            logs.showSuccessLog('[WallCore] '+DEVICE_DESC+' server is listening on '+DEVICE_IP+':'+DEVICE_PORT);
              timerIntervalConfig = setInterval(requestIntervalConfig,millisecsInterval)
              timerInterval = setInterval(requestInterval,DEVICE_TIME_REQUEST);  
                    
          })
        }else{
          app.listen(DEVICE_PORT, (err) => {
            if (err) {
              return logs.showErrorLog('something bad happened on initialize server: '+err);
            }
            logs.showSuccessLog('[WallCore] '+DEVICE_DESC+' is listening on '+DEVICE_IP+':'+DEVICE_PORT);
          })
        }
      }else{
        logs.showErrorLog("Can't load config.ini #01");
        setConfigFile();
      }
    }catch(e){
      logs.showErrorLog("Can't load config.ini #02 "+e);
      setConfigFile();
    }

  };
  
  initWallCore();