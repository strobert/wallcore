const colors = require('colors/safe');

  const showLog = (msg) =>{
    console.log(msg)
  };
  const showConfigLog = (msg) =>{
    console.log(colors.bgBlue("[CONFIG] "+msg));
  };
  const showRequestsLog = (msg)=>{
    console.log(colors.yellow("[REQUESTS] "+msg))
  };
  const  showErrorLog = (msg) => {
    console.log(colors.red("[ERROR] "+msg))
  }
  const  showSuccessLog = (msg) => {
    console.log(colors.green("[SUCCESS] "+msg))
  }
module.exports= {
    showLog,
    showConfigLog,
    showRequestsLog,
    showErrorLog,
    showSuccessLog
}