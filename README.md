# WallCore ![CI status](https://img.shields.io/badge/Version-0.0.8-green.svg)

A node server that creates a configuration file and makes requests to a web service in php that returns the new videos contained in a folder in the server for its later download. After the download, it validates each video file by checksum.


### Development Requirements
* NodeJS and npm
* node modules:

`$ npm install express`

`$ npm install request`

`$ npm install node-cmd`

`$ npm install -g pkg`

`$ pip install colors`

`$ pip install ini`

`$ pip install fs`

`$ pip install crypto`

## Installation
```
npm install
npm install -g pkg
```
## Development Run
```
$ npm start
```

## Rebuild (all platforms)
```
$ pkg wallcore.js

```
## Usage


Just run the binary in the foldier /builds (wallcore-linux, wallcore-macos, wallcore-win.exe)
